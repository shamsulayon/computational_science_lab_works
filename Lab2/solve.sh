        
echo "##########################################################"> result.txt
echo "solution in [-8, 8]">> result.txt
echo "3-exp(x)">> result.txt
./solve py_flib.py my_exp -8 8 >> result.txt
echo "trigonometry">> result.txt
./solve py_flib.py my_trigonometry -8 8  >> result.txt
echo "cubic">> result.txt
./solve py_flib.py my_cubic -8 8  >> result.txt
echo "gamma">> result.txt
./solve py_flib.py my_gamma -8 8  >> result.txt

echo "\n\n">>result.txt
echo "##########################################################">> result.txt
echo "solution in [0,2]">> result.txt
echo "3-exp(x)">> result.txt
./solve py_flib.py my_exp 0 2 >> result.txt
echo "trigonometry">> result.txt
./solve py_flib.py my_trigonometry  0 2  >> result.txt
echo "cubic">> result.txt
./solve py_flib.py my_cubic  0 2  >> result.txt
echo "gamma">> result.txt
./solve py_flib.py my_gamma  0 2  >> result.txt
